﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace BryggeriGUI
{
    public partial class Form1 : Form
    {
        string CSVFileName = "";
        SaveFileDialog _sfd = null;
        SaveFileDialog sfd {
            get {
                if (_sfd == null) {
                    _sfd = new SaveFileDialog();
                    _sfd.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                    _sfd.FilterIndex = 2;
                    _sfd.RestoreDirectory = true;
                }
                return _sfd;
            }
        }
        PackageCommunicator pc = null;
        DateTime loggingStarted;

        public Form1()
        {
            InitializeComponent();
        }

        private void WriteMessage(string message)
        {
            this.BeginInvoke((MethodInvoker)delegate {
                InfoTextBox.AppendText(message);
            });
        }

        private void SerialComboBox_DropDown(object sender, EventArgs e)
        {
            SerialComboBox.Items.Clear();
            SerialComboBox.Items.AddRange(SerialPort.GetPortNames());
        }

        void getFlags()
        {
            pc.TransmitPackage(new Package(PackageType_t.PT_GET, new byte[] { (byte)PackageValue_t.PV_FLAG }));
        }

        private void ConnectButton_Click(object sender, EventArgs e)
        {
            if (pc == null)
            {
              //  try
                //{
                    loggingStarted = DateTime.Now;
                    pc = new PackageCommunicator(SerialComboBox.Text, 9600, interpretPackage);
                    pc.messageWriter = WriteMessage;
                    ConnectButton.Text = "Disconnect";
                getFlags();
                Thread.Sleep(100);
                    getPIDValues();
                Thread.Sleep(100);
                getSetPoint();
                    enableButtons(true);
                    clearSeries();
              /* }
                catch (Exception ex) {
                    MessageBox.Show("Error: " + ex.Message, "Something went wrong");
                }*/
            } else {
                pc.Close();
                pc = null;
                ConnectButton.Text = "Connect";
                enableButtons(false);
            }
        }

        private void enableButtons(bool v)
        {
            SerialComboBox.Enabled = !v;
            EnablePIDCheckBox.Enabled = v;
            PIDFormComboBox.Enabled = v;
            //if (EnablePIDCheckBox.Checked) {
            KpTextBox.Enabled = v;
            TiTextBox.Enabled = v;
            TdTextBox.Enabled = v;

            //}
            PIDGetButton.Enabled = v;
            PIDSetButton.Enabled = v;
            SetPointTextBox.Enabled = v;
            SPGetButton.Enabled = v;
            SPSetButton.Enabled = v;
            PowerTextBox.Enabled = v;
            PowerSetButton.Enabled = v;
            AutoUpdateCheckBox.Enabled = v;
            SaveCSVCheckBox.Enabled = v;
            MeasurementCheckBox.Enabled = v;
        }


        private void interpretPackage(Package p)
        {
            this.BeginInvoke((MethodInvoker)delegate {
                InfoTextBox.AppendText("Package: "+p.ToString() + " successfully received\r\n");
                /*for (int i = 0; i < p.payload.Length; i++) {
                    InfoTextBox.AppendText(p.payload[i].ToString("X2") + " ");
                }*/
                //InfoTextBox.AppendText("\r\n");
            });
            switch (p.Type) {
                case PackageType_t.PT_DATA:
                    interpretDataPackage(p.payload);
                    break;
                case PackageType_t.PT_CONFIRM:
                    this.BeginInvoke((MethodInvoker)delegate
                    {
                        InfoTextBox.AppendText(((PackageValue_t)p.payload[0]).ToString() + "\r\n");
                    });
                        break;
            }
        }

        private void interpretDataPackage(byte[] payload)
        {
            DateTime timeNow = DateTime.Now;

            PackageValue_t pv = (PackageValue_t)payload[0];
            switch (pv)
            {
                case PackageValue_t.PV_PID:
                    PID_t pid = new PID_t(payload, 1);
                    this.BeginInvoke((MethodInvoker)delegate { 
                        InfoTextBox.AppendText(pid.ToString() + "\r\n");
                        KpTextBox.Text = pid.Kp.ToString();
                        if (pid.Kp != 0.0)
                        {
                            TiTextBox.Text = (pid.Kp / pid.Ki).ToString();
                            TdTextBox.Text = (pid.Kd / pid.Kp).ToString();
                        } else {
                            TiTextBox.Text = "";
                            TdTextBox.Text = "";
                        }
                    });
                    break;
                case PackageValue_t.PV_PROCESS:
                    Process_t proc = new Process_t(payload, 1);
                    double t = (timeNow - loggingStarted).TotalSeconds;
                    this.BeginInvoke((MethodInvoker)delegate { 
                        InfoTextBox.AppendText(proc.ToString() + "\r\n");
                        
                        chart1.Series["Temperature"].Points.AddXY(t, proc.Pv / 16.0);
                        chart1.Series["Power"].Points.AddXY(t, proc.u);
                        chart1.Series["I"].Points.AddXY(t, proc.I);
                        chart1.Series["D"].Points.AddXY(t, proc.D);
                        chart1.Series["Set point"].Points.AddXY(t, proc.Sp / 16.0);
                        if (EnablePIDCheckBox.Checked) {
                            PowerTextBox.Text = proc.u.ToString();
                        }
                    });

                    if (CSVFileName != "")
                    {
                        try
                        {
                            string line = "";
                            line += t.ToString();
                            line += "\t" + (proc.Pv/16.0).ToString();
                            line += "\t" + proc.u.ToString();
                            line += "\t" + proc.I.ToString();
                            line += "\t" + proc.D.ToString();
                            line += "\t" + (proc.Sp / 16.0).ToString();
                            File.AppendAllText(CSVFileName, line + "\r\n");
                        }
                        catch (Exception ex)
                        {
                            InfoTextBox.AppendText("Error writing to CSV file: " + CSVFileName + " (" + ex.Message + ")");
                        }
                    }

                    break;
                case PackageValue_t.PV_MESSAGE:
                    this.BeginInvoke((MethodInvoker)delegate {
                        InfoTextBox.AppendText("Message: ");
                        InfoTextBox.AppendText(Encoding.GetEncoding("437").GetString(payload, 1, payload.Length-1));
                        InfoTextBox.AppendText("\r\n");
                    });
                    break;
                case PackageValue_t.PV_FLAG:
                    this.BeginInvoke((MethodInvoker)delegate {
                        ushort flags = (ushort)(payload[1] | (payload[2] << 8));
                        if ((flags & (ushort)Flags_t.FLAG_AUTO_TRANSMIT_PACKAGES) != 0) {
                            AutoUpdateCheckBox.Checked = true;
                        } else
                        {
                            AutoUpdateCheckBox.Checked = false;
                        }
                        if ((flags & (ushort)Flags_t.FLAG_PID_ENABLE) != 0)
                        {
                            EnablePIDCheckBox.Checked = true;
                        }
                        else
                        {
                            EnablePIDCheckBox.Checked = false;
                        }

                        string message = "";
                        for (int i = 0; i < 4; i++) {
                            message += ((Flags_t)(1 << i)).ToString() + "=" + (flags & (1 << i)).ToString() + "\r\n";
                        }
                        InfoTextBox.AppendText(message);
                    });
                        break;
                case PackageValue_t.PV_SETPOINT:
                    this.BeginInvoke((MethodInvoker)delegate
                    {
                        SetPointTextBox.Text = (BitConverter.ToSingle(payload, 1)/16.0).ToString();
                    });
                    break;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (pc != null) pc.Close();
            pc = null;
        }

        private void MeasurementCheckBox_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (e.NewValue == CheckState.Checked)
            {
                chart1.Series[e.Index].Enabled = true;
            }
            else {
                chart1.Series[e.Index].Enabled = false;
            }
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            enableButtons(false);
            for (int i = 0; i < 5; i++) {
                chart1.Series[i].Enabled = false;
            }
            MeasurementCheckBox.SetItemChecked(0, true);
            MeasurementCheckBox.SetItemChecked(4, true);
        }

        void getPIDValues()
        {
            pc.TransmitPackage(new Package(PackageType_t.PT_GET, new byte[] { (byte)PackageValue_t.PV_PID }));
        }

        private void PIDGetButton_Click(object sender, EventArgs e)
        {
            getPIDValues();
        }

        private void PIDSetButton_Click(object sender, EventArgs e)
        {
            
            try
            {
                byte[] payload = new byte[21];
                payload[0] = (byte)PackageValue_t.PV_PID;
                float Kp = float.Parse(KpTextBox.Text);
                float Ki = Kp/float.Parse(TiTextBox.Text);
                float Kd = Kp*float.Parse(TdTextBox.Text);
                BitConverter.GetBytes(Kp).CopyTo(payload, 1);
                BitConverter.GetBytes(Ki).CopyTo(payload, 5);
                BitConverter.GetBytes(Kd).CopyTo(payload, 9);
                BitConverter.GetBytes(0.0f).CopyTo(payload, 13);
                BitConverter.GetBytes(1000.0f).CopyTo(payload, 17);
                pc.TransmitPackage(new Package(PackageType_t.PT_SET, payload));
            }
            catch (Exception ex) {
                MessageBox.Show("Something went wrong. Probably something with the format of PID regulator parameters. (" + ex.Message + ")");
            }
            
        }

        private void EnablePIDCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (EnablePIDCheckBox.Checked)
            {
                pc.SetFlag(Flags_t.FLAG_PID_ENABLE);
                Thread.Sleep(100);
                pc.TransmitPackage(new Package(PackageType_t.PT_SET, new byte[] { (byte)PackageValue_t.PV_RESETPROCESS }));
                PowerSetButton.Enabled = false;
                PowerTextBox.Enabled = false;
                
            }
            else {
                pc.ClearFlag(Flags_t.FLAG_PID_ENABLE);
                PowerSetButton.Enabled = true;
                PowerTextBox.Enabled = true;
            }
        }

        private void AutoUpdateCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (AutoUpdateCheckBox.Checked)
            {
                pc.SetFlag(Flags_t.FLAG_AUTO_TRANSMIT_PACKAGES);
            } else {
                pc.ClearFlag(Flags_t.FLAG_AUTO_TRANSMIT_PACKAGES);
            }
        }

        void getSetPoint()
        {
            pc.TransmitPackage(new Package(PackageType_t.PT_GET, new byte[] { (byte)PackageValue_t.PV_SETPOINT }));
        }

        private void SPGetButton_Click(object sender, EventArgs e)
        {
            getSetPoint();
        }

        private void SPSetButton_Click(object sender, EventArgs e)
        {
            
            try
            {
                byte[] payload = new byte[5];
                payload[0] = (byte)PackageValue_t.PV_SETPOINT;
                BitConverter.GetBytes(16.0f*float.Parse(SetPointTextBox.Text)).CopyTo(payload, 1);
                pc.TransmitPackage(new Package(PackageType_t.PT_SET, payload));
            }
            catch (Exception ex) {
                MessageBox.Show("Something went wrong while processing setting set point: " + ex.Message);
            }
        }

        void clearSeries()
        {
            foreach (Series ser in chart1.Series)
            {
                ser.Points.Clear();
            }
        }

        private void chart1_DoubleClick(object sender, EventArgs e)
        {
            clearSeries();
        }

        private void flagsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            getFlags();
        }

        private void avsluttToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void SaveCSVCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (SaveCSVCheckBox.Checked)
            {
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    CSVFileName = sfd.FileName;
                    clearSeries();
                    loggingStarted = DateTime.Now;
                }
            } else {
                CSVFileName = "";
            }
        }

        private void PIDResetIButton_Click(object sender, EventArgs e)
        {
            
        }
    }
}
