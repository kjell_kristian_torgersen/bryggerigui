﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BryggeriGUI
{
    class Process_t
    {
        public float Pv { get; set; }
        public float Sp { get; set; }
        public float u { get; set; }
        public float I { get; set; }
        public float D { get; set; }
        public float I_last { get; set; }
        public float Pv_last { get; set; }
        
        public Process_t(byte[] bytes, int offset) {
            Pv = BitConverter.ToSingle(bytes, 0 + offset);
            Sp = BitConverter.ToSingle(bytes, 4 + offset);
            u = BitConverter.ToSingle(bytes, 8 + offset);
            I = BitConverter.ToSingle(bytes, 12 + offset);
            D = BitConverter.ToSingle(bytes, 16 + offset);
            I_last = BitConverter.ToSingle(bytes, 20 + offset);
            Pv_last = BitConverter.ToSingle(bytes, 24 + offset);
        }

        public override string ToString()
        {
            string info = "";
            info += "I=" + I.ToString();
            info += " D=" + D.ToString();
            info += " I_last=" + I_last.ToString();
            info += " Pv_last=" + Pv_last.ToString();
            info += " Pv=" + (Pv / 16.0).ToString();
            info += " Sp=" + (Sp / 16.0).ToString();
            info += " u=" + (u).ToString();
            return info;
        }
    }
}
