﻿namespace BryggeriGUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.filToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.velgSerieportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.avsluttToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.InfoTextBox = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.PIDSetButton = new System.Windows.Forms.Button();
            this.PIDGetButton = new System.Windows.Forms.Button();
            this.PowerSetButton = new System.Windows.Forms.Button();
            this.PowerTextBox = new System.Windows.Forms.TextBox();
            this.PowerLabel = new System.Windows.Forms.Label();
            this.EnablePIDCheckBox = new System.Windows.Forms.CheckBox();
            this.SPSetButton = new System.Windows.Forms.Button();
            this.SPGetButton = new System.Windows.Forms.Button();
            this.SetPointTextBox = new System.Windows.Forms.TextBox();
            this.SetPointLabel = new System.Windows.Forms.Label();
            this.TdTextBox = new System.Windows.Forms.TextBox();
            this.TdLabel = new System.Windows.Forms.Label();
            this.TiTextBox = new System.Windows.Forms.TextBox();
            this.TiLabel = new System.Windows.Forms.Label();
            this.KpTextBox = new System.Windows.Forms.TextBox();
            this.KpLabel = new System.Windows.Forms.Label();
            this.PIDFormComboBox = new System.Windows.Forms.ComboBox();
            this.MeasurementCheckBox = new System.Windows.Forms.CheckedListBox();
            this.SaveCSVCheckBox = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SerialComboBox = new System.Windows.Forms.ComboBox();
            this.ConnectButton = new System.Windows.Forms.Button();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.AutoUpdateCheckBox = new System.Windows.Forms.CheckBox();
            this.miscToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.flagsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PIDResetIButton = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.filToolStripMenuItem,
            this.miscToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(807, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // filToolStripMenuItem
            // 
            this.filToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.velgSerieportToolStripMenuItem,
            this.avsluttToolStripMenuItem});
            this.filToolStripMenuItem.Name = "filToolStripMenuItem";
            this.filToolStripMenuItem.Size = new System.Drawing.Size(29, 20);
            this.filToolStripMenuItem.Text = "Fil";
            // 
            // velgSerieportToolStripMenuItem
            // 
            this.velgSerieportToolStripMenuItem.Name = "velgSerieportToolStripMenuItem";
            this.velgSerieportToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.velgSerieportToolStripMenuItem.Text = "Velg serieport";
            // 
            // avsluttToolStripMenuItem
            // 
            this.avsluttToolStripMenuItem.Name = "avsluttToolStripMenuItem";
            this.avsluttToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.avsluttToolStripMenuItem.Text = "Avslutt";
            this.avsluttToolStripMenuItem.Click += new System.EventHandler(this.avsluttToolStripMenuItem_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel1.Controls.Add(this.splitContainer1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 24);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(807, 632);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.chart1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.InfoTextBox);
            this.splitContainer1.Size = new System.Drawing.Size(681, 626);
            this.splitContainer1.SplitterDistance = 427;
            this.splitContainer1.TabIndex = 0;
            // 
            // chart1
            // 
            chartArea1.AxisY.IsStartedFromZero = false;
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            this.chart1.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(0, 0);
            this.chart1.Name = "chart1";
            this.chart1.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Pastel;
            series1.BorderWidth = 2;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series1.Legend = "Legend1";
            series1.Name = "Temperature";
            series2.BorderWidth = 2;
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series2.Legend = "Legend1";
            series2.Name = "Power";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Legend = "Legend1";
            series3.Name = "I";
            series4.BorderWidth = 2;
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series4.Legend = "Legend1";
            series4.Name = "D";
            series5.BorderWidth = 2;
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series5.Legend = "Legend1";
            series5.Name = "Set point";
            this.chart1.Series.Add(series1);
            this.chart1.Series.Add(series2);
            this.chart1.Series.Add(series3);
            this.chart1.Series.Add(series4);
            this.chart1.Series.Add(series5);
            this.chart1.Size = new System.Drawing.Size(681, 427);
            this.chart1.TabIndex = 20;
            this.chart1.Text = "chart1";
            this.chart1.DoubleClick += new System.EventHandler(this.chart1_DoubleClick);
            // 
            // InfoTextBox
            // 
            this.InfoTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InfoTextBox.Location = new System.Drawing.Point(0, 0);
            this.InfoTextBox.Multiline = true;
            this.InfoTextBox.Name = "InfoTextBox";
            this.InfoTextBox.ReadOnly = true;
            this.InfoTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.InfoTextBox.Size = new System.Drawing.Size(681, 195);
            this.InfoTextBox.TabIndex = 21;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.PIDResetIButton);
            this.panel1.Controls.Add(this.AutoUpdateCheckBox);
            this.panel1.Controls.Add(this.PIDSetButton);
            this.panel1.Controls.Add(this.PIDGetButton);
            this.panel1.Controls.Add(this.PowerSetButton);
            this.panel1.Controls.Add(this.PowerTextBox);
            this.panel1.Controls.Add(this.PowerLabel);
            this.panel1.Controls.Add(this.EnablePIDCheckBox);
            this.panel1.Controls.Add(this.SPSetButton);
            this.panel1.Controls.Add(this.SPGetButton);
            this.panel1.Controls.Add(this.SetPointTextBox);
            this.panel1.Controls.Add(this.SetPointLabel);
            this.panel1.Controls.Add(this.TdTextBox);
            this.panel1.Controls.Add(this.TdLabel);
            this.panel1.Controls.Add(this.TiTextBox);
            this.panel1.Controls.Add(this.TiLabel);
            this.panel1.Controls.Add(this.KpTextBox);
            this.panel1.Controls.Add(this.KpLabel);
            this.panel1.Controls.Add(this.PIDFormComboBox);
            this.panel1.Controls.Add(this.MeasurementCheckBox);
            this.panel1.Controls.Add(this.SaveCSVCheckBox);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.SerialComboBox);
            this.panel1.Controls.Add(this.ConnectButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(690, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(114, 626);
            this.panel1.TabIndex = 1;
            // 
            // PIDSetButton
            // 
            this.PIDSetButton.Location = new System.Drawing.Point(55, 229);
            this.PIDSetButton.Name = "PIDSetButton";
            this.PIDSetButton.Size = new System.Drawing.Size(48, 23);
            this.PIDSetButton.TabIndex = 12;
            this.PIDSetButton.Text = "Set";
            this.PIDSetButton.UseVisualStyleBackColor = true;
            this.PIDSetButton.Click += new System.EventHandler(this.PIDSetButton_Click);
            // 
            // PIDGetButton
            // 
            this.PIDGetButton.Location = new System.Drawing.Point(4, 229);
            this.PIDGetButton.Name = "PIDGetButton";
            this.PIDGetButton.Size = new System.Drawing.Size(45, 23);
            this.PIDGetButton.TabIndex = 11;
            this.PIDGetButton.Text = "Get";
            this.PIDGetButton.UseVisualStyleBackColor = true;
            this.PIDGetButton.Click += new System.EventHandler(this.PIDGetButton_Click);
            // 
            // PowerSetButton
            // 
            this.PowerSetButton.Location = new System.Drawing.Point(4, 426);
            this.PowerSetButton.Name = "PowerSetButton";
            this.PowerSetButton.Size = new System.Drawing.Size(105, 23);
            this.PowerSetButton.TabIndex = 19;
            this.PowerSetButton.Text = "Set";
            this.PowerSetButton.UseVisualStyleBackColor = true;
            // 
            // PowerTextBox
            // 
            this.PowerTextBox.Location = new System.Drawing.Point(3, 400);
            this.PowerTextBox.Name = "PowerTextBox";
            this.PowerTextBox.Size = new System.Drawing.Size(100, 20);
            this.PowerTextBox.TabIndex = 18;
            // 
            // PowerLabel
            // 
            this.PowerLabel.AutoSize = true;
            this.PowerLabel.Location = new System.Drawing.Point(3, 384);
            this.PowerLabel.Name = "PowerLabel";
            this.PowerLabel.Size = new System.Drawing.Size(37, 13);
            this.PowerLabel.TabIndex = 17;
            this.PowerLabel.Text = "Power";
            // 
            // EnablePIDCheckBox
            // 
            this.EnablePIDCheckBox.AutoSize = true;
            this.EnablePIDCheckBox.Checked = true;
            this.EnablePIDCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.EnablePIDCheckBox.Location = new System.Drawing.Point(6, 101);
            this.EnablePIDCheckBox.Name = "EnablePIDCheckBox";
            this.EnablePIDCheckBox.Size = new System.Drawing.Size(80, 17);
            this.EnablePIDCheckBox.TabIndex = 3;
            this.EnablePIDCheckBox.Text = "Enable PID";
            this.EnablePIDCheckBox.UseVisualStyleBackColor = true;
            this.EnablePIDCheckBox.CheckedChanged += new System.EventHandler(this.EnablePIDCheckBox_CheckedChanged);
            // 
            // SPSetButton
            // 
            this.SPSetButton.Location = new System.Drawing.Point(55, 343);
            this.SPSetButton.Name = "SPSetButton";
            this.SPSetButton.Size = new System.Drawing.Size(48, 23);
            this.SPSetButton.TabIndex = 16;
            this.SPSetButton.Text = "Set";
            this.SPSetButton.UseVisualStyleBackColor = true;
            this.SPSetButton.Click += new System.EventHandler(this.SPSetButton_Click);
            // 
            // SPGetButton
            // 
            this.SPGetButton.Location = new System.Drawing.Point(4, 343);
            this.SPGetButton.Name = "SPGetButton";
            this.SPGetButton.Size = new System.Drawing.Size(45, 23);
            this.SPGetButton.TabIndex = 15;
            this.SPGetButton.Text = "Get";
            this.SPGetButton.UseVisualStyleBackColor = true;
            this.SPGetButton.Click += new System.EventHandler(this.SPGetButton_Click);
            // 
            // SetPointTextBox
            // 
            this.SetPointTextBox.Location = new System.Drawing.Point(4, 317);
            this.SetPointTextBox.Name = "SetPointTextBox";
            this.SetPointTextBox.Size = new System.Drawing.Size(100, 20);
            this.SetPointTextBox.TabIndex = 14;
            // 
            // SetPointLabel
            // 
            this.SetPointLabel.AutoSize = true;
            this.SetPointLabel.Location = new System.Drawing.Point(1, 301);
            this.SetPointLabel.Name = "SetPointLabel";
            this.SetPointLabel.Size = new System.Drawing.Size(49, 13);
            this.SetPointLabel.TabIndex = 13;
            this.SetPointLabel.Text = "Set point";
            // 
            // TdTextBox
            // 
            this.TdTextBox.Location = new System.Drawing.Point(29, 203);
            this.TdTextBox.Name = "TdTextBox";
            this.TdTextBox.Size = new System.Drawing.Size(71, 20);
            this.TdTextBox.TabIndex = 10;
            // 
            // TdLabel
            // 
            this.TdLabel.AutoSize = true;
            this.TdLabel.Location = new System.Drawing.Point(7, 206);
            this.TdLabel.Name = "TdLabel";
            this.TdLabel.Size = new System.Drawing.Size(20, 13);
            this.TdLabel.TabIndex = 9;
            this.TdLabel.Text = "Td";
            // 
            // TiTextBox
            // 
            this.TiTextBox.Location = new System.Drawing.Point(29, 177);
            this.TiTextBox.Name = "TiTextBox";
            this.TiTextBox.Size = new System.Drawing.Size(71, 20);
            this.TiTextBox.TabIndex = 8;
            // 
            // TiLabel
            // 
            this.TiLabel.AutoSize = true;
            this.TiLabel.Location = new System.Drawing.Point(7, 180);
            this.TiLabel.Name = "TiLabel";
            this.TiLabel.Size = new System.Drawing.Size(16, 13);
            this.TiLabel.TabIndex = 7;
            this.TiLabel.Text = "Ti";
            // 
            // KpTextBox
            // 
            this.KpTextBox.Location = new System.Drawing.Point(29, 151);
            this.KpTextBox.Name = "KpTextBox";
            this.KpTextBox.Size = new System.Drawing.Size(71, 20);
            this.KpTextBox.TabIndex = 6;
            // 
            // KpLabel
            // 
            this.KpLabel.AutoSize = true;
            this.KpLabel.Location = new System.Drawing.Point(7, 154);
            this.KpLabel.Name = "KpLabel";
            this.KpLabel.Size = new System.Drawing.Size(20, 13);
            this.KpLabel.TabIndex = 5;
            this.KpLabel.Text = "Kp";
            // 
            // PIDFormComboBox
            // 
            this.PIDFormComboBox.FormattingEnabled = true;
            this.PIDFormComboBox.Items.AddRange(new object[] {
            "Standard form (industry standard)",
            "Ideal form (for theoretical stuff)"});
            this.PIDFormComboBox.Location = new System.Drawing.Point(3, 124);
            this.PIDFormComboBox.Name = "PIDFormComboBox";
            this.PIDFormComboBox.Size = new System.Drawing.Size(105, 21);
            this.PIDFormComboBox.TabIndex = 4;
            // 
            // MeasurementCheckBox
            // 
            this.MeasurementCheckBox.CheckOnClick = true;
            this.MeasurementCheckBox.FormattingEnabled = true;
            this.MeasurementCheckBox.Items.AddRange(new object[] {
            "Temperature",
            "Power",
            "I",
            "D",
            "Set point"});
            this.MeasurementCheckBox.Location = new System.Drawing.Point(3, 529);
            this.MeasurementCheckBox.Name = "MeasurementCheckBox";
            this.MeasurementCheckBox.Size = new System.Drawing.Size(99, 94);
            this.MeasurementCheckBox.TabIndex = 21;
            this.MeasurementCheckBox.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.MeasurementCheckBox_ItemCheck);
            // 
            // SaveCSVCheckBox
            // 
            this.SaveCSVCheckBox.AutoSize = true;
            this.SaveCSVCheckBox.Location = new System.Drawing.Point(0, 491);
            this.SaveCSVCheckBox.Name = "SaveCSVCheckBox";
            this.SaveCSVCheckBox.Size = new System.Drawing.Size(74, 17);
            this.SaveCSVCheckBox.TabIndex = 20;
            this.SaveCSVCheckBox.Text = "Save .csv";
            this.SaveCSVCheckBox.UseVisualStyleBackColor = true;
            this.SaveCSVCheckBox.CheckedChanged += new System.EventHandler(this.SaveCSVCheckBox_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Serial port";
            // 
            // SerialComboBox
            // 
            this.SerialComboBox.FormattingEnabled = true;
            this.SerialComboBox.Location = new System.Drawing.Point(4, 29);
            this.SerialComboBox.Name = "SerialComboBox";
            this.SerialComboBox.Size = new System.Drawing.Size(107, 21);
            this.SerialComboBox.TabIndex = 1;
            this.SerialComboBox.DropDown += new System.EventHandler(this.SerialComboBox_DropDown);
            // 
            // ConnectButton
            // 
            this.ConnectButton.Location = new System.Drawing.Point(4, 56);
            this.ConnectButton.Name = "ConnectButton";
            this.ConnectButton.Size = new System.Drawing.Size(107, 23);
            this.ConnectButton.TabIndex = 2;
            this.ConnectButton.Text = "Connect";
            this.ConnectButton.UseVisualStyleBackColor = true;
            this.ConnectButton.Click += new System.EventHandler(this.ConnectButton_Click);
            // 
            // AutoUpdateCheckBox
            // 
            this.AutoUpdateCheckBox.AutoSize = true;
            this.AutoUpdateCheckBox.Checked = true;
            this.AutoUpdateCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.AutoUpdateCheckBox.Location = new System.Drawing.Point(0, 468);
            this.AutoUpdateCheckBox.Name = "AutoUpdateCheckBox";
            this.AutoUpdateCheckBox.Size = new System.Drawing.Size(84, 17);
            this.AutoUpdateCheckBox.TabIndex = 22;
            this.AutoUpdateCheckBox.Text = "Auto update";
            this.AutoUpdateCheckBox.UseVisualStyleBackColor = true;
            this.AutoUpdateCheckBox.CheckedChanged += new System.EventHandler(this.AutoUpdateCheckBox_CheckedChanged);
            // 
            // miscToolStripMenuItem
            // 
            this.miscToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.flagsToolStripMenuItem});
            this.miscToolStripMenuItem.Name = "miscToolStripMenuItem";
            this.miscToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.miscToolStripMenuItem.Text = "Misc";
            // 
            // flagsToolStripMenuItem
            // 
            this.flagsToolStripMenuItem.Name = "flagsToolStripMenuItem";
            this.flagsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.flagsToolStripMenuItem.Text = "Flags...";
            this.flagsToolStripMenuItem.Click += new System.EventHandler(this.flagsToolStripMenuItem_Click);
            // 
            // PIDResetIButton
            // 
            this.PIDResetIButton.Location = new System.Drawing.Point(3, 258);
            this.PIDResetIButton.Name = "PIDResetIButton";
            this.PIDResetIButton.Size = new System.Drawing.Size(54, 23);
            this.PIDResetIButton.TabIndex = 23;
            this.PIDResetIButton.Text = "Reset I";
            this.PIDResetIButton.UseVisualStyleBackColor = true;
            this.PIDResetIButton.Click += new System.EventHandler(this.PIDResetIButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(807, 656);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem filToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem velgSerieportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem avsluttToolStripMenuItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.TextBox InfoTextBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button PowerSetButton;
        private System.Windows.Forms.TextBox PowerTextBox;
        private System.Windows.Forms.Label PowerLabel;
        private System.Windows.Forms.CheckBox EnablePIDCheckBox;
        private System.Windows.Forms.Button SPSetButton;
        private System.Windows.Forms.Button SPGetButton;
        private System.Windows.Forms.TextBox SetPointTextBox;
        private System.Windows.Forms.Label SetPointLabel;
        private System.Windows.Forms.TextBox TdTextBox;
        private System.Windows.Forms.Label TdLabel;
        private System.Windows.Forms.TextBox TiTextBox;
        private System.Windows.Forms.Label TiLabel;
        private System.Windows.Forms.TextBox KpTextBox;
        private System.Windows.Forms.Label KpLabel;
        private System.Windows.Forms.ComboBox PIDFormComboBox;
        private System.Windows.Forms.CheckedListBox MeasurementCheckBox;
        private System.Windows.Forms.CheckBox SaveCSVCheckBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox SerialComboBox;
        private System.Windows.Forms.Button ConnectButton;
        private System.Windows.Forms.FontDialog fontDialog1;
        private System.Windows.Forms.Button PIDSetButton;
        private System.Windows.Forms.Button PIDGetButton;
        private System.Windows.Forms.CheckBox AutoUpdateCheckBox;
        private System.Windows.Forms.ToolStripMenuItem miscToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem flagsToolStripMenuItem;
        private System.Windows.Forms.Button PIDResetIButton;
    }
}

