﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows.Forms;

namespace BryggeriGUI
{
    delegate void PackageInterpreter(Package p);
    delegate void MessageWriter(string message);

    class PackageCommunicator
    {
        SerialPort sp;
        PackageInterpreter interpretPackage = null;
        public MessageWriter messageWriter { get; set; } = null;

        int packageIndex = 0;
        ushort packageChecksum = 0;
        byte[] package = null;

        public void SetFlag(Flags_t flag)
        {
            this.TransmitPackage(new Package(PackageType_t.PT_SET, new byte[] { (byte)PackageValue_t.PV_SETFLAG, (byte)((int)flag & 0xFF), (byte)(((int)flag >> 8) & 0xFF) }));
        }

        public void ClearFlag(Flags_t flag)
        {
            this.TransmitPackage(new Package(PackageType_t.PT_SET, new byte[] { (byte)PackageValue_t.PV_CLEARFLAG, (byte)((int)flag & 0xFF), (byte)(((int)flag >> 8) & 0xFF) }));
        }
        public void TransmitPackage(Package p)
        {
            string message = "TX: ";
            byte[] package = p.toBytes();
            for (int i = 0; i < package.Length; i++)
            {
                message += package[i].ToString("X2") + " ";
            }
            Debug.WriteLine(message);

            try
            {
                if (sp.IsOpen)
                {
                    sp.Write(package, 0, package.Length);

                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error transmitting package: " + ex.Message + " (" + p.ToString() + ")");
            }
        }

        public PackageCommunicator(string comPort, int baudRate, PackageInterpreter interpretPackage)
        {
            sp = new SerialPort(comPort, baudRate, Parity.None, 8, StopBits.One);
            this.interpretPackage = interpretPackage;
            sp.DataReceived += Sp_DataReceived;
            sp.ErrorReceived += Sp_ErrorReceived;
            sp.ReceivedBytesThreshold = 1;
            sp.WriteTimeout = 500;
            sp.ReadTimeout = 0;
            sp.Open();
            sp.DiscardInBuffer();
            //TransmitPackage(new Package(PackageType_t.PT_GET, new byte[] { (byte)PackageValue_t.PV_FLAG }));
        }

        private void Sp_ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            //MessageBox.Show("Serial port error: " + e.ToString());
        }

        public void Close()
        {
            if (sp.IsOpen) sp.Close();
        }

        void processPackage(byte b)
        {
            switch (packageIndex)
            {
                case 0:
                    if (b == Package.START_BYTE)
                    {
                        packageIndex = 1;
                        packageChecksum = b;
                    }
                    else
                    {
                        if (messageWriter != null)
                        {
                            /*var e = Encoding.GetEncoding("437");
                            byte[] a = new byte[1];
                            a[0] = b;
                            messageWriter(e.GetString(a));*/
                        }
                    }
                    break;
                case 1:
                    package = new byte[(int)b + 3];
                    package[0] = (byte)Package.START_BYTE;
                    package[1] = b;
                    packageChecksum += b;
                    packageIndex = 2;
                    break;
                default:
                    package[packageIndex] = b;
                    packageChecksum += b;
                    packageIndex++;
                    if (packageIndex >= package[1] + 3)
                    {
                        packageIndex = 0;
                        while (packageChecksum > 0x100)
                        {
                            packageChecksum = (ushort)((packageChecksum >> 8)
                                    + (packageChecksum & 0xFF));
                        }
                        packageChecksum = (ushort)((~packageChecksum) & 0xFF);
                        if (packageChecksum == 0)
                        {
                            if (interpretPackage != null) interpretPackage(new Package(package));
                        }
                        else
                        {
                            if (messageWriter != null)
                            {
                                messageWriter("CS fail for package: ");
                                for (int i = 0; i < package.Length; i++)
                                {
                                    messageWriter(package[i].ToString("X2") + " ");
                                }
                                messageWriter("\r\n");
                            }
                        }
                    }
                    break;
            }
        }

        private void Sp_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            byte b;
            int c;
            string message = "RX: ";
            int n = sp.BytesToRead;
            for(int i = 0; i < n; i++)
            {
                c = sp.ReadByte();
                if (c != -1)
                {
                    b = (byte)c;
                    message += b.ToString("X2") + " ";
                    processPackage(b);
                }
            }
            Debug.WriteLine(message);
        }
    }
}
