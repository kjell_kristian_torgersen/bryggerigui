﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BryggeriGUI
{
    class PID_t
    {
        public float Kp { get; set; }
        public float Ki { get; set; }
        public float Kd { get; set; }
        public float u_min { get; set; }
        public float u_max { get; set; }

        public PID_t(byte[] bytes, int offset)
        {    
            Kp = BitConverter.ToSingle(bytes, 0 + offset);
            Ki = BitConverter.ToSingle(bytes, 4 + offset);
            Kd = BitConverter.ToSingle(bytes, 8 + offset);
            u_min = BitConverter.ToSingle(bytes, 12 + offset);
            u_max = BitConverter.ToSingle(bytes, 16 + offset);
        }

        public override string ToString()
        {
            string info = "";
            info += "Kp=" + Kp.ToString();
            info += " Ki=" + Ki.ToString();
            info += " Kd=" + Kd.ToString();
            info += " u_min=" + u_min.ToString();
            info += " u_max=" + u_max.ToString();
            return info;
        }
    }
}
