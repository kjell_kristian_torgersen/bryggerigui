﻿namespace BryggeriGUI
{
    public enum PackageType_t { PT_GET = 0, PT_SET = 1, PT_DATA = 2, PT_CONFIRM = 3 }
    public enum Flags_t { FLAG_UARTRX_OVERRUN=1, FLAG_UARTTX_OVERRUN=2, FLAG_PID_ENABLE	=4, FLAG_AUTO_TRANSMIT_PACKAGES=8}
    public enum PackageValue_t { PV_PID, PV_PROCESS, PV_AUTOTRANSMIT, PV_MESSAGE, PV_SETFLAG , PV_CLEARFLAG , PV_FLAG, PV_SETPOINT, PV_RESETPROCESS }

    public class Package
    {
        public static int START_BYTE = 0xAA;

        public PackageType_t Type { get; set; }
        public byte[] payload { get; set; } = null;

        public Package(byte[] package)
        {
            payload = new byte[package[1]-1];
            Type = (PackageType_t)package[2];
            for (int i = 0; i < payload.Length; i++) {
                payload[i] = package[3 + i];
            }
        }

        public Package(PackageType_t packageType, byte[] payload)
        {
            this.Type = packageType;
            this.payload = payload;
        }

        public static byte getChecksum(byte[] package)
        {
            ushort checksum = 0;
            for (int i = 0; i < package.Length; i++) {
                checksum += package[i];
                while (checksum >= 256) {
                    checksum -= 255;
                }
            }
            return (byte)((~checksum) & 0xFF);
        }

        public byte[] toBytes() {
            byte[] package = new byte[payload.Length + 4];

            package[0] = (byte)START_BYTE;
            package[1] = (byte)(payload.Length + 1);
            package[2] = (byte)Type;
            for (int i = 0; i < payload.Length; i++) {
                package[3 + i] = payload[i];
            }
            package[3 + payload.Length] = 0;
            package[3 + payload.Length] = getChecksum(package);
            return package;
        }

        public override string ToString()
        {
            
            string info= "TYPE=" + Type.ToString() + " SIZE=" + payload.Length;
            if (Type == PackageType_t.PT_DATA) {
                PackageValue_t pv = (PackageValue_t)payload[0];
                info += " VALUE=" + pv.ToString();
            }
            return info;
        }

    }
}